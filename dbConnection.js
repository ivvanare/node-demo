const Sequelize = require('sequelize')

const sequelize = new Sequelize('p0003_test','root','', {
  host: 'localhost',
  dialect: 'mysql',
})

/**
 * Test connection to database
 */
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

module.exports = sequelize;
