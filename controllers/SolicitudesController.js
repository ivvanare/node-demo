// 'use strict'

// var xml2js   = require('xml2js');
// var parser   = new xml2js.Parser();

// Cargamos el modelo para usarlo posteriormente
var solicitudModel = require('../models/solicitud');
 
// Creamos un método en el controlador, en este caso una accion de pruebas
function index(req, res){

  // Solicitud.findAll({ attributes: ['id', 'title', 'link', 'description', 'keywords'] }) by atributes
  
  solicitudModel.findAll()
  .then(result => {
    console.log(result)

    // Devolvemos una re spuesta en JSON
    res.status(200).send(result)
  })
  .catch(err => {
    console.log(err)
    //res.send(xml(err));
  })
}
 
function save(req, res, next){

  res.set('Content-Type', 'text/xml');

  var data = req.body.padre;

  console.log("data of save:" + req.body)

  // Create a new solicitud
  solicitudModel.create({
		identificador_emisor: data.identificador_emisor._text,
		banco_emisor: data.banco_emisor._text,
		identificador_receptor: data.identificador_receptor._text,
		banco_receptor: data.banco_receptor._text,
		monto: parseFloat(data.monto._text)
  }).then(result => {
    res.send(`<result><status>ok<status><message>Solicitud saved wiht ID:${result.id}</message></result>`);
  }).catch(err => {
    console.log(err);
    res.status(500).json(err);
  })

}


// Exportamos las funciones en un objeto json para poder usarlas en otros fuera de este fichero
module.exports = {
  index,
  save
};