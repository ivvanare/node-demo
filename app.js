var express = require('express'),
    bodyParser = require('body-parser');

require('body-parser-xml-json')(bodyParser);

var app = express();
app.use(bodyParser.xml({
  xmlParseOptions: {
    compact: true 
  }
}));

// Importamos las rutas
var solicitudes_route = require('./routes/api/solicitudesRoute');

// Cargamos las rutas con el prefijo api
app.use('/api', solicitudes_route);

app.listen(3000, () => {
  console.log("El servidor está inicializado en el puerto http://localhost:3000");
});
