'use strict'
 
// Cargamos el módulo de express para poder crear rutas
var express = require('express');
 
// Cargamos el controlador
var SolicitudesController = require('../../controllers/SolicitudesController');
 
// Llamamos al router
var apiRoute = express.Router();
 
// Creamos una ruta de tipo GET para el método de pruebas
apiRoute.get('/solicitudes', SolicitudesController.index);
apiRoute.post('/solicitudes/save', SolicitudesController.save);
 
// Exportamos la configuración
module.exports = apiRoute;
