/**
 Model file of a Solicitud
*/
const Sequelize = require('sequelize')

var sequelize = require('../dbConnection')

const Solicitud = sequelize.define('solicitudes', {
  id: {
    type: Sequelize.SMALLINT, 
    primaryKey: true,
    autoIncrement: true,
  },
  identificador_emisor: Sequelize.STRING,
  banco_emisor: Sequelize.STRING,
  identificador_receptor: Sequelize.STRING,
  banco_receptor: Sequelize.STRING,
  monto: Sequelize.STRING,
},{
  timestamps: false
});

module.exports = Solicitud;
